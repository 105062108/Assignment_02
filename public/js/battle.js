var player;
var player2;
var keyboard;

var items = [];

var leftWall;
var rightWall;
var ceiling;
var first_platform;

var floor = 1;
var cur_life, cur_stage, cur_life_2;

//sound effects
var music_se, jump_se, crack_se;

var mainState = {
    preload: function () {
        game.load.image('bg', 'assets/bg.png');
        game.load.image('normal', 'assets/normal.png');
        game.load.image('nails', 'assets/nails.png');
        game.load.image('my_canvas', 'assets/canvas.png');
        game.load.image('forum', 'assets/forum.png');
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
        game.load.image('wall', 'assets/wall.png');
        game.load.image('wall2', 'assets/wall2.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.spritesheet('player', 'assets/player.png', 32, 32);
        game.load.spritesheet('player2', 'assets/player2.png', 32, 32);
        game.load.audio('bgm', ['assets/bgm.mp3', 'assets/bgm.ogg']);
        game.load.audio('jump', ['assets/jump1.mp3', 'assets/jump1.ogg']);
        game.load.audio('crack', ['assets/crack.mp3', 'assets/crack.ogg']);
    },

    create: function () {
        game.add.image(0, 0, 'bg');
        //bgm
        music_se = game.add.audio('bgm');
        music_se.play();
        music_se.volume = 0.2;
        music_se.loop = true;

        //sound effects
        jump_se = game.add.audio('jump');
        jump_se.volume = 0.1;
        jump_se.loop = false;
        crack_se = game.add.audio('crack');
        crack_se.volume = 0.2;
        crack_se.loop = false;

        //player
        player = game.add.sprite(325, 250, 'player');
        player.frame = 8;
        game.physics.arcade.enable(player);
        player.body.gravity.y = 500;
        player.anchor.setTo(0.5, 0.5);
        player.scale.setTo(1.2, 1.2);
        player.animations.add('stand', [8], 8);
        player.animations.add('leftmove', [0, 1, 2, 3], 12);
        player.animations.add('rightmove', [9, 10, 11, 12], 12);
        player.animations.add('leftjump', [18, 19, 20, 21], 12);
        player.animations.add('rightjump', [27, 28, 29, 30], 12);
        player.animations.add('drop', [36, 37, 38, 39], 12);
        player.life = 10;
        player.touchOn = undefined;

        //player2
        player2 = game.add.sprite(275, 250, 'player2');
        player2.frame = 8;
        game.physics.arcade.enable(player2);
        player2.body.gravity.y = 500;
        player2.anchor.setTo(0.5, 0.5);
        player2.scale.setTo(1.2, 1.2);
        player2.animations.add('stand', [8], 8);
        player2.animations.add('leftmove', [0, 1, 2, 3], 12);
        player2.animations.add('rightmove', [9, 10, 11, 12], 12);
        player2.animations.add('leftjump', [18, 19, 20, 21], 12);
        player2.animations.add('rightjump', [27, 28, 29, 30], 12);
        player2.animations.add('drop', [36, 37, 38, 39], 12);
        player2.life = 10;
        player2.touchOn = undefined;

        //keyboard
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'W': Phaser.Keyboard.W,
            'S': Phaser.Keyboard.S,
            'A': Phaser.Keyboard.A,
            'D': Phaser.Keyboard.D
        });

        //ceiling
        ceiling = game.add.sprite(0, 0, 'ceiling');
        ceiling.scale.setTo(1.5, 1.5);
        game.physics.arcade.enable(ceiling);
        ceiling.body.immovable = true;

        //leftWall
        leftWall = game.add.sprite(0, 0, 'wall2');
        leftWall.scale.setTo(1.5, 2.0);
        game.physics.arcade.enable(leftWall);
        leftWall.body.immovable = true;

        //rightWall
        rightWall = game.add.sprite(574, 0, 'wall');
        rightWall.scale.setTo(1.5, 2.0);
        game.physics.arcade.enable(rightWall);
        rightWall.body.immovable = true;
        //first platform
        first_platform = game.add.sprite(250, 690, 'normal');
        first_platform.scale.setTo(1.2, 1.2);
        game.physics.arcade.enable(first_platform);
        first_platform.body.immovable = true;
        items.push(first_platform);
        first_platform.body.checkCollision.down = false;

        //text
        var style = { fill: 'salmon', fontSize: '40px' }
        cur_life = game.add.text(460, 30, '', style);
        cur_stage = game.add.text(280, 30, '', style);
        cur_life_2 = game.add.text(40, 30, '', style);

    },

    update: function () {
        checkGameOver();
        createItems();
        moveItems();
        if (player.body != null) movePlayer();
        if (player2.body != null) movePlayer2();
        updateStatus();
        checkFloor();
        if (player.body != null) {
            this.physics.arcade.collide(player, items, effect);
            this.physics.arcade.collide(player, leftWall);
            this.physics.arcade.collide(player, rightWall);
            this.physics.arcade.collide(player, ceiling, checkCeiling);
        }
        //player2
        if (player2.body != null) {
            this.physics.arcade.collide(player2, items, effect2);
            this.physics.arcade.collide(player2, leftWall);
            this.physics.arcade.collide(player2, rightWall);
            this.physics.arcade.collide(player2, ceiling, checkCeiling2);
        }
        if (player.body != null && player2.body != null) {
            this.physics.arcade.collide(player, player2);
        }
    },
};

var cur_time = 0;
var next_time = 600;
function createItems() {
    if (game.time.now > cur_time + next_time) {
        cur_time = game.time.now;
        generateItem();
    }
}

var floor_time = 0;
var next_floor_time = 2000;
function checkFloor() {
    if (game.time.now > floor_time + next_floor_time) {
        floor_time = game.time.now;
        floor += 1;
    }
}

function generateItem() {
    var item;
    var rand = Math.random() * 100;
    var x = 50 + Math.random() * (550 - 174);
    next_time = 600 + Math.random() * 400;

    if (rand < 25) {
        item = game.add.sprite(x, 750, 'normal');
    } else if (rand < 45) {
        item = game.add.sprite(x, 750, 'nails');
        game.physics.arcade.enable(item);
        item.body.setSize(96, 15, 0, 15);
    } else if (rand < 52.5) {
        item = game.add.sprite(x, 750, 'conveyorLeft');
        item.animations.add('scroll', [0, 1, 2, 3], 16, true);
        item.play('scroll');
    } else if (rand < 60) {
        item = game.add.sprite(x, 750, 'conveyorRight');
        item.animations.add('scroll', [0, 1, 2, 3], 16, true);
        item.play('scroll');
    } else if (rand < 80) {
        item = game.add.sprite(x, 750, 'trampoline');
        item.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        item.frame = 3;
    } else {
        item = game.add.sprite(x, 750, 'fake');
        item.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }
    item.scale.setTo(1.2, 1.2);
    game.physics.arcade.enable(item);
    item.body.immovable = true;
    items.push(item);

    item.body.checkCollision.down = false;
}

function moveItems() {
    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        item.body.position.y -= 2;
        if (item.body.position.y <= -20) {
            item.destroy();
            items.splice(i, 1);
        }
    }
}

function movePlayer() {
    if (player.body != null) {
        if (keyboard.left.isDown) {
            player.body.velocity.x = -250;
        } else if (keyboard.right.isDown) {
            player.body.velocity.x = 250;
        } else {
            player.body.velocity.x = 0;
        }
        if (keyboard.down.isDown) {
            if (player.body.velocity.y < 0)
                player.body.velocity.y = 0;
        }
        moveAnimation(player);
    }
}

function movePlayer2() {
    if (player2.body != null) {
        if (keyboard.A.isDown) {
            player2.body.velocity.x = -250;
        } else if (keyboard.D.isDown) {
            player2.body.velocity.x = 250;
        } else {
            player2.body.velocity.x = 0;
        }
        if (keyboard.S.isDown) {
            if (player2.body.velocity.y < 0)
                player2.body.velocity.y = 0;
        }
        moveAnimation2(player2);
    }
}

function moveAnimation(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;

    if (x == -250 && y < 8) {
        player.animations.play('leftjump');
    }
    else if (x == 250 && y < 8) {
        player.animations.play('rightjump');
    }
    else if (x == -250 && y > 8 && y < 9) {
        player.animations.play('leftmove');
    }
    else if (x == 250 && y > 8 && y < 9) {
        player.animations.play('rightmove');
    }
    else if (x == 0 && y > 8 && y < 9) {
        player.animations.play('stand');
    }
    else {
        player.animations.play('drop');
    }
}

function moveAnimation2(player2) {
    var x = player2.body.velocity.x;
    var y = player2.body.velocity.y;

    if (x == -250 && y < 8) {
        player2.animations.play('leftjump');
    }
    else if (x == 250 && y < 8) {
        player2.animations.play('rightjump');
    }
    else if (x == -250 && y > 8 && y < 9) {
        player2.animations.play('leftmove');
    }
    else if (x == 250 && y > 8 && y < 9) {
        player2.animations.play('rightmove');
    }
    else if (x == 0 && y > 8 && y < 9) {
        player2.animations.play('stand');
    }
    else {
        player2.animations.play('drop');
    }
}

function effect(player, item) {
    if (item.key == 'normal') {
        if (player.touchOn != item) {
            player.touchOn = item;
            if (player.life < 10)
                player.life += 1;
        }
    }
    if (item.key == 'nails') {
        if (player.touchOn != item) {
            player.touchOn = item;
            player.life -= 3;
        }
    }
    if (item.key == 'conveyorLeft') {
        if (player.touchOn != item)
            player.touchOn = item;
        player.body.x -= 2;
    }
    if (item.key == 'conveyorRight') {
        if (player.touchOn != item)
            player.touchOn = item;
        player.body.x += 2;
    }
    if (item.key == 'trampoline') {
        if (player.touchOn != item)
            player.touchOn = item;
        item.animations.play('jump');
        player.body.velocity.y = -500;
        jump_se.play();
    }
    if (item.key == 'fake') {
        item.animations.play('turn');
        if (item.body.checkCollision.up == true)
            crack_se.play();
        setTimeout(function () {
            item.body.checkCollision.up = false;
        }, 100);
    }
}

function effect2(player2, item) {
    if (item.key == 'normal') {
        if (player2.touchOn != item) {
            player2.touchOn = item;
            if (player2.life < 10)
                player2.life += 1;
        }
    }
    if (item.key == 'nails') {
        if (player2.touchOn != item) {
            player2.touchOn = item;
            player2.life -= 3;
        }
    }
    if (item.key == 'conveyorLeft') {
        if (player2.touchOn != item)
            player2.touchOn = item;
        player2.body.x -= 2;
    }
    if (item.key == 'conveyorRight') {
        if (player2.touchOn != item)
            player2.touchOn = item;
        player2.body.x += 2;
    }
    if (item.key == 'trampoline') {
        if (player2.touchOn != item)
            player2.touchOn = item;
        item.animations.play('jump');
        player2.body.velocity.y = -500;
        jump_se.play();
    }
    if (item.key == 'fake') {
        item.animations.play('turn');
        if (item.body.checkCollision.up == true)
            crack_se.play();
        setTimeout(function () {
            item.body.checkCollision.up = false;
        }, 100);
    }
}

function checkCeiling(player, ceiling) {
    player.life -= 3;
    if (player.body != null)
        player.body.y = 50;
}

function checkCeiling2(player2, ceiling) {
    player2.life -= 3;
    if (player2.body != null)
        player2.body.y = 50;
}

function updateStatus() {
    if (player.body != null && player.body.y < 800)
        cur_life.setText('life:' + player.life);
    else cur_life.setText('dead');
    if (player2.body != null && player2.body.y < 800)
        cur_life_2.setText('life:' + player2.life);
    else cur_life_2.setText('dead');
    cur_stage.setText('B' + floor);
}

function checkGameOver() {
    if(player.body.y > 1000 || player2.body.y > 1000 || player.life <= 0 || player2.life <= 0) {
        gameOver();
    }
}

function gameOver() {
    items.forEach(function (s) { s.destroy() });
    items = [];
    game.paused = true;
    location.href = 'gg2.html';
    bgm.stop();
}

var game = new Phaser.Game(600, 750, Phaser.AUTO, 'canvas');
game.state.add('main', mainState);
game.state.start('main');



