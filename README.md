# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed 

# Report
## 實作項目
|                                              Todo                                              | Y/N |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  Y  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  Y  |
|         All things in your game should have correct physical properties and behaviors.         |  Y  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  Y  |
| Add some additional sound effects and UI to enrich your game.                                  |  Y  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  N  |
| Appearance (subjective)                                                                        |  Y  |
| Other creative features in your game (describe on README.md)                                   |  Y  |
## 開發歷程
* 這次的作業，參考了許多網路上以及 `phaser` 官網的資源。遊戲主體放在`game.js`裡面，以下簡單介紹一下使用到的函式：
    * `createItems`, `generateItem`
        隨機生成平台，有8種，分別是：
        一般平台 (25%)，碰到或站上此平台會加一滴血（滿血 10）。
        尖刺平台 (20%)，碰到或站上此平台會扣三滴血。
        輸送帶－左 (7.5%)，站在上面會被往左位移。 
        輸送帶－右 (7.5%)，站在上面會被往右位移。 
        彈簧平台 (20%)，碰觸到此平台，人物會往上彈跳。
        虛偽平台 (15%)，這個平台上站不了人，不過可以當作緩衝。
        小畫家平台 (2.5%)，碰到這個平台會直接跳轉到之前做的小畫家。
        論壇平台 (2.5%)，碰到這個平台會直接跳轉到之前做的論壇。
    * `moveItems`
        使平台不斷往上移動，這樣才像小朋友下樓梯。
        平台超出頁面時直接銷毀，省空間。
    * `movePlayer`, `moveAnimation`
        使玩家可以控制角色移動。
        左右鍵控制角色向左或向右移動。
        角色向上彈跳時，按下鍵可以使角色更早落地。
        根據角色目前的移動狀態，播放不同的動畫。
    * `effect`
        實作碰觸不同平台時會觸發的各種效果。
        `items`是一個陣列，用來紀錄當前在畫面中的平台。
        透過拿取每個`item`的`key`來給予它們不同的性質。
        使用`touchOn`判斷當前碰觸到的是哪一種平台。
    * `checkCeiling`
        頂端的尖刺一直存在，所以沒有寫在生成平台的函式裡，獨立撰寫。
        碰到頂端尖刺時扣三滴血。
    * `updateStatus`
        更新畫面上給玩家看的數據（角色血量、目前層數）。
        使用了`setText`來實作。
    * `checkFloor`
        用來記錄層數。
        這邊設計成每過兩秒會增加一層。
    * `checkGameOver`
        檢查遊戲是否已經結束。
        如果遊戲結束，銷毀所有平台，停止播放音樂，並跳轉到結束畫面。

## 特色實作
* 鍵盤下鍵運用得當，可以使玩家控制跳躍高度。
* 有雙人對戰模式，可以兩個人 PK 下樓梯。
    * `1P` 使用 `A、S、D` 鍵。
    * `2P` 使用`左、下、右`鍵。
* 小畫家平台和論壇平台的實作，用另一種方式呈現以往的作品，也為這個遊戲增加了一些趣味性與難度。
    * 這兩個平台的圖片非長方形，但模組為長方形，玩家容易誤觸而亡。
* 取消原作的無敵時間系統，改用單次碰撞判定是否重複扣血。
* 彈簧與尖刺之間的微妙關係，使這個遊戲更偏向技術取向的遊戲。
    * 若操作不當，很有可能在一瞬間因重複碰撞尖刺而死亡。

